import {PlayerModel} from "./player.model";

export interface TeamModel {
    id: string,
    name: string,
    shirtColor: string,
    championship: string,
    leaguePlace: number,
    players?: PlayerModel[]
}
