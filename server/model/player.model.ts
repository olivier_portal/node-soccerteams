export interface PlayerModel {
    id: string,
    firstName: string,
    lastName: string,
    playerPosition: string,
    playerNumber: number,
    playerState: PlayerState,
}

export enum PlayerState {
    selected = 'titulaire',
    substitute = 'remplaçant',
    onTouch = 'sur le banc',
    injured = 'blessé',
}
