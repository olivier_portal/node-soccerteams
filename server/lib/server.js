"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cert_1 = require("./cred/cert");
const firebase_admin_1 = __importDefault(require("firebase-admin"));
firebase_admin_1.default.initializeApp({
    credential: firebase_admin_1.default.credential.cert(cert_1.cert),
    databaseURL: "https://node-lab-d228c.firebaseio.com"
});
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const teams_service_1 = require("./service/teams.service");
const players_service_1 = require("./service/players.service");
const app = express_1.default();
app.use(cors_1.default());
app.use(body_parser_1.default.json());
// Ajouter une nouvelle team=========================================================
app.post('/api/teams', (req, res) => {
    try {
        const newTeam = req.body;
        const addResult = teams_service_1.postNewTeam(newTeam);
        return res.send(addResult);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur', } + e.message);
    }
});
app.post('/api/teams/:teamId/players', async (req, res) => {
    try {
        const teamId = req.params.teamId;
        const newPlayer = req.body;
        const result = await players_service_1.addNewPlayerinTeam(teamId, newPlayer);
        return res.send(result);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur', } + e.message);
    }
});
// Récupérer des players dans une team par leurs uid=========================================================
app.get('/api/teams/:teamId/players/:playerId', async (req, res) => {
    try {
        const player = await players_service_1.getPlayersInTeamByUids([
            '2oUfM1cQ5goHrd3dcfqA',
            '55X5T3ob6zKydF2xsw1g',
            '8sNMKG5ixBkoTNHvyvDx'
        ]);
        return res.send(player);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur', } + e.message);
    }
});
// Supprimer un player d'une Team
app.delete('/api/teams/:teamId/players/:playerId', async (req, res) => {
    try {
        const teamId = req.params.teamId;
        const playerId = req.params.playerId;
        const result = await players_service_1.suppressPlayerInTeam(teamId, playerId);
        return res.send(result);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur', } + e.message);
    }
});
// Ajouter un player existant d'une Team
app.put('/api/teams/:teamId/players/:playerId', async (req, res) => {
    try {
        const teamId = req.params.teamId;
        const playerId = req.params.playerId;
        const result = await players_service_1.addExistingPlayerInTeam(teamId, playerId);
        return res.send(result);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur', } + e.message);
    }
});
// Transférer un player existant d'une Team
/*app.put('/api/teams/:teamAUid/:teamBUid/players/:playerToTransferUid', async (req, res) => {
    try {
        const teamAUId: string = req.params.teamAUid;
        const teamBUid: string = req.params.teamBUid;
        const playerToTransferUid: string = req.params.playerToTransferUid;
        const result = await transferPlayerFromAToBTeam(teamAUId, teamBUid, playerToTransferUid)
        return res.send(result);
    } catch (e){
        return res.status(500).send({error: 'erreur serveur',} + e.message);
    }
});*/
app.delete('/api/teams/:teamAUid/players/:playerToTransferUid', async (req, res) => {
    try {
        const teamAUId = req.params.teamAUid;
        const playerToTransferUid = req.params.playerToTransferUid;
        const result = await players_service_1.transferPlayerFromAToBTeam(teamAUId, teamBUId, playerToTransferUid);
        return res.send(result);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur', } + e.message);
    }
});
app.put('/api/teams/:teamBUid/players/:playerToTransferUid', async (req, res) => {
    try {
        const teamBUId = req.params.teamBUId;
        const playerToTransferUid = req.params.playerToTransferUid;
        const result = await players_service_1.transferPlayerFromAToBTeam(teamAUId, teamBUId, playerToTransferUid);
        return res.send(result);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur', } + e.message);
    }
});
app.patch('/api/teams/:teamId', async (req, res) => {
    try {
        const teamId = req.params.teamId;
        const newTeam = req.body;
        const result = await teams_service_1.teamToPatch(teamId, newTeam);
        return res.send(result);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur', } + e.message);
    }
});
app.put('/api/teams/:teamId', async (req, res) => {
    try {
        const teamId = req.params.teamId;
        const newTeam = req.body;
        const result = await teams_service_1.teamToPut(teamId, newTeam);
        return res.send(result);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur', } + e.message);
    }
});
app.patch('/api/players/:playerId', async (req, res) => {
    try {
        const playerId = req.params.playerId;
        const state = req.body.playerState;
        const result = await players_service_1.setPlayerByState(playerId, state);
        return res.send(result);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur', } + e.message);
    }
});
// Récupérer toutes les équipes
app.get('/api/teams', async (req, res) => {
    try {
        const teams = await teams_service_1.getAllTeams();
        return res.send(teams);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur' + e.message });
    }
});
app.get('/api/teams/:id', async (req, res) => {
    try {
        const teamId = req.params.id;
        const teamToFind = await teams_service_1.getTeamByIdWithAllPlayers(teamId);
        return res.send(teamToFind);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur' + e.message });
    }
});
// Supprimer une Team
app.delete('/api/teams/:teamId', async (req, res) => {
    try {
        const teamId = req.params.teamId;
        const result = await teams_service_1.deleteTeam(teamId);
        return res.send(result);
    }
    catch (e) {
        return res.status(500).send({ error: 'erreur serveur', } + e.message);
    }
});
app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});
//# sourceMappingURL=server.js.map