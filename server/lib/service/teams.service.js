"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = __importDefault(require("firebase-admin"));
const players_service_1 = require("./players.service");
const db = firebase_admin_1.default.firestore();
const teamsCollection = db.collection('teams');
async function findTeamById(teams, teamId) {
    if (!teamId) {
        throw new Error('teamId does not exist');
    }
    return teams.find((team) => teamId === team.id);
}
exports.findTeamById = findTeamById;
// Get all teams =========================================================
async function getAllTeams() {
    const teamToQuerySnap = await teamsCollection.get();
    const hostels = [];
    teamToQuerySnap.forEach(teamSnap => hostels.push(teamSnap.data()));
    return hostels;
}
exports.getAllTeams = getAllTeams;
// Post newTeam ================================================================
async function postNewTeam(newTeam) {
    if (!newTeam) {
        throw new Error('newTeam must be filled');
    }
    const addResult = await teamsCollection.add(newTeam);
    return Object.assign(Object.assign({}, newTeam), { id: addResult.id });
}
exports.postNewTeam = postNewTeam;
// Patch team =======================================================================
async function teamToPatch(teamId, newTeam) {
    if (!teamId || !newTeam) {
        throw new Error('teamId and newTeam must be filled');
    }
    const teamRef = teamsCollection.doc(teamId);
    const teamRefSnapToPatch = await teamRef.get();
    const teamToPatch = teamRefSnapToPatch.data();
    if (!teamToPatch) {
        throw new Error('This team does not exist');
    }
    await teamRef.update(newTeam);
    return 'ok';
}
exports.teamToPatch = teamToPatch;
// Put team =======================================================================
async function teamToPut(teamId, newTeam) {
    if (!teamId || !newTeam) {
        throw new Error('teamId and newTeam must be filled');
    }
    const teamRef = teamsCollection.doc(teamId);
    const teamRefSnapToPatch = await teamRef.get();
    const teamToPut = teamRefSnapToPatch.data();
    if (!teamToPut) {
        throw new Error('This team does not exist');
    }
    await teamRef.set(newTeam);
    return 'ok';
}
exports.teamToPut = teamToPut;
// Get team by Id =======================================================================
async function getTeamById(teamId) {
    if (!teamId) {
        throw new Error(`teamId required`);
    }
    const teamToFindRef = teamsCollection.doc(teamId);
    const teamToFindSnap = await teamToFindRef.get();
    if (!teamToFindSnap.exists) {
        throw new Error(`hostel ${teamId} does not exist`);
    }
    else {
        return teamToFindSnap.data();
    }
}
exports.getTeamById = getTeamById;
// Get team by Id with all players=======================================================================
async function getTeamByIdWithAllPlayers(teamId) {
    const teamToFind = await getTeamById(teamId);
    const playersCollectionInTeam = teamsCollection
        .doc(teamId)
        .collection('players');
    const playersCollectionInTeamSnap = await playersCollectionInTeam.get();
    const payersRef = [];
    playersCollectionInTeamSnap.forEach((playerRef) => payersRef.push(playerRef.data().uid));
    teamToFind.players = await players_service_1.getAllPlayersByUids(payersRef);
    return teamToFind;
}
exports.getTeamByIdWithAllPlayers = getTeamByIdWithAllPlayers;
// Delete team by Id=======================================================================
async function deleteTeam(teamId) {
    if (!teamId) {
        throw new Error(`This team doesn't exist`);
    }
    const teamRef = teamsCollection.doc(teamId);
    const snapTeamToDelete = await teamRef.get();
    const teamToDelete = snapTeamToDelete.data();
    if (!teamToDelete) {
        return `The hostel with the id ${teamId} doesn't exist`;
    }
    await teamRef.delete();
    return { response: `The hostel with the id ${teamId} has been deleted successfully` };
}
exports.deleteTeam = deleteTeam;
//# sourceMappingURL=teams.service.js.map