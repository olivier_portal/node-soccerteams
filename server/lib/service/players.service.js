"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const firebase_admin_1 = __importDefault(require("firebase-admin"));
const player_model_1 = require("../model/player.model");
const db = firebase_admin_1.default.firestore();
const teamCollection = db.collection('teams');
const playerCollection = db.collection('players');
// Récupérer un player par son uid =========================================================
async function getPayerByUid(playerId) {
    if (!playerId) {
        throw new Error('playerId does not exist');
    }
    const playerRef = playerCollection.doc(playerId);
    const playerRefSnap = await playerRef.get();
    if (!playerRefSnap) {
        throw new Error(`The player with the id ${playerId} does'nt exist`);
    }
    return await playerRefSnap.data();
}
exports.getPayerByUid = getPayerByUid;
// Récupérer des players dans une team par leurs uid=========================================================
async function getPlayersInTeamByUids(playersUids) {
    const promisesToResolve = playersUids.map((uid) => getPayerByUid(uid));
    return Promise.all(promisesToResolve);
}
exports.getPlayersInTeamByUids = getPlayersInTeamByUids;
// Ajouter un nouveau player dans une team =========================================================
async function addNewPlayerinTeam(teamId, newPlayer) {
    if (!teamId || !newPlayer) {
        throw new Error('teamId or newPlayer is missing');
    }
    const teamRef = teamCollection.doc(teamId);
    const teamRefSnap = await teamRef.get();
    if (!teamRefSnap) {
        throw new Error('team does not exist');
    }
    const createPlayerRef = await playerCollection.add(newPlayer);
    const createPlayerRefInTeamRef = teamRef
        .collection('players')
        .doc(createPlayerRef.id);
    await createPlayerRefInTeamRef.set({ ref: createPlayerRef, uid: createPlayerRef.id });
    return 'ok';
}
exports.addNewPlayerinTeam = addNewPlayerinTeam;
// Supprimer un player d'une team =======================================================
async function suppressPlayerInTeam(teamId, playerId) {
    if (!teamId || !playerId) {
        throw new Error('teamId and playerId are required');
    }
    const teamRef = teamCollection.doc(teamId);
    const teamRefSnap = await teamRef.get();
    if (!teamRefSnap) {
        throw new Error('team does not exist');
    }
    const playerRefToDelete = playerCollection.doc(playerId);
    const playerRefToDeleteSnap = await playerRefToDelete.get();
    const playerToDelete = playerRefToDeleteSnap.data();
    if (!playerToDelete) {
        throw new Error(`The player with the id ${playerId} does'nt exist`);
    }
    const playerRefToDeleteInTeamRef = teamRef
        .collection('players')
        .doc(playerRefToDelete.id);
    await playerRefToDeleteInTeamRef.delete();
    return 'ok';
}
exports.suppressPlayerInTeam = suppressPlayerInTeam;
// Ajouter un player existant dans une team =======================================================
async function addExistingPlayerInTeam(teamId, playerId) {
    if (!teamId || !playerId) {
        throw new Error('teamId and playerId are required');
    }
    const teamRef = teamCollection.doc(teamId);
    const teamRefSnap = await teamRef.get();
    if (!teamRefSnap) {
        throw new Error('team does not exist');
    }
    const playerRefToAdd = playerCollection.doc(playerId);
    const playerRefToAddSnap = await playerRefToAdd.get();
    const playerToAdd = playerRefToAddSnap.data();
    if (!playerToAdd) {
        throw new Error(`The player with the id ${playerId} does'nt exist`);
    }
    const playerRefToAddInTeamRef = teamRef
        .collection('players')
        .doc(playerRefToAdd.id);
    await playerRefToAddInTeamRef.set({ ref: playerRefToAdd, uid: playerRefToAdd.id });
    return 'ok';
}
exports.addExistingPlayerInTeam = addExistingPlayerInTeam;
// Transférer un player d'une team A à B =======================================================
async function transferPlayerFromAToBTeam(teamAUid, teamBUid, playerToTransferUid) {
    // Vérifier que toutes les données sont renseignées
    if (!teamAUid || !teamBUid || !playerToTransferUid) {
        throw new Error(`teamAUid, teamBUid and playerToTransferUid are all required`);
    }
    // Récupérer l'uid de la team A
    const teamARef = teamCollection.doc(teamAUid);
    const teamARefSnap = await teamARef.get();
    if (!teamARefSnap) {
        throw new Error(`teamA doesn't exist`);
    }
    // Récupérer l'uid de la team B
    const teamBRef = teamCollection.doc(teamAUid);
    const teamBRefSnap = await teamBRef.get();
    if (!teamBRefSnap) {
        throw new Error(`teamB doesn't exist`);
    }
    // Récupérer l'uid du joueur à transférer
    const playerToTransferRef = playerCollection.doc(playerToTransferUid);
    const playerToTransferSnap = await playerToTransferRef.get();
    const playerToTransfer = playerToTransferSnap.data();
    if (!playerToTransfer) {
        throw new Error(`The player with the id ${playerToTransferUid} does'nt exist`);
    }
    // Supprimer le joueur de la team A
    const playerRefToDeleteInTeamARef = teamARef
        .collection('players')
        .doc(playerToTransferRef.id);
    await playerRefToDeleteInTeamARef.delete();
    // Ajouter le joueur à la team B
    const playerRefToAddInTeamBRef = teamBRef
        .collection('players')
        .doc(playerToTransferRef.id);
    await playerRefToAddInTeamBRef.set({ ref: playerToTransferRef, uid: playerToTransferRef.id });
    return 'ok';
}
exports.transferPlayerFromAToBTeam = transferPlayerFromAToBTeam;
// Mettre à jour le statut d'un player =======================================================
// vérifier que le statut existe
function checkPlayerState(state) {
    return Object.keys(player_model_1.PlayerState).some((playerState) => playerState === state);
}
exports.checkPlayerState = checkPlayerState;
// Faire un patch du state
async function setPlayerByState(playerId, state) {
    if (!playerId) {
        throw new Error(`PlayerId is missing`);
    }
    if (!checkPlayerState(state)) {
        throw new Error('State invalid');
    }
    const playerRef = playerCollection.doc(playerId);
    const playerRefSnap = await playerRef.get();
    if (!playerRefSnap) {
        throw new Error(`The player with the id ${playerId} does'nt exist`);
    }
    await playerRef.update({ playerState: state });
    return 'ok';
}
exports.setPlayerByState = setPlayerByState;
async function getPlayerById(playerId) {
    if (!playerId) {
        throw new Error('playerId is missing');
    }
    const playerRef = playerCollection.doc(playerId);
    const playerSnap = await playerRef.get();
    if (!playerSnap.exists) {
        throw new Error(`room ${playerId} does not exist`);
    }
    return playerSnap.data();
}
exports.getPlayerById = getPlayerById;
async function getAllPlayersByUids(playersUids) {
    const promisesToResolve = playersUids
        .map((uid) => getPlayerById(uid));
    return await Promise.all(promisesToResolve);
}
exports.getAllPlayersByUids = getAllPlayersByUids;
//# sourceMappingURL=players.service.js.map