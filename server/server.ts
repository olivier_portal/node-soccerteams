import {cert} from "./cred/cert";
import admin from 'firebase-admin';

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://node-lab-d228c.firebaseio.com"
});

import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import {
    deleteTeam,
    getAllTeams, getTeamById, getTeamByIdWithAllPlayers,
    postNewTeam,
    teamToPatch,
    teamToPut
} from "./service/teams.service";
import {PlayerModel, PlayerState} from "./model/player.model";
import {
    addExistingPlayerInTeam,
    addNewPlayerinTeam,
    getPlayersInTeamByUids,
    setPlayerByState,
    suppressPlayerInTeam, transferPlayerFromAToBTeam,
} from "./service/players.service";
import {TeamModel} from "./model/team.model";


const app = express();
app.use(cors());
app.use(bodyParser.json());

// Ajouter une nouvelle team=========================================================

app.post('/api/teams', (req, res) => {
    try {
        const newTeam = req.body;
        const addResult = postNewTeam(newTeam);
        return res.send(addResult);
    } catch (e){
        return res.status(500).send({error: 'erreur serveur',} + e.message);
    }
});


app.post('/api/teams/:teamId/players', async (req, res) => {
    try {
        const teamId: string = req.params.teamId;
        const newPlayer: PlayerModel = req.body;
        const result = await addNewPlayerinTeam(teamId, newPlayer)
        return res.send(result);
    } catch (e){
        return res.status(500).send({error: 'erreur serveur',} + e.message);
    }
});

// Récupérer des players dans une team par leurs uid=========================================================

app.get('/api/teams/:teamId/players/:playerId', async (req, res) => {
    try {
        const player = await getPlayersInTeamByUids(
          [
              '2oUfM1cQ5goHrd3dcfqA',
              '55X5T3ob6zKydF2xsw1g',
              '8sNMKG5ixBkoTNHvyvDx'
          ]
        );
        return res.send(player);
    } catch (e){
        return res.status(500).send({error: 'erreur serveur',} + e.message);
    }
});

// Supprimer un player d'une Team

app.delete('/api/teams/:teamId/players/:playerId', async (req, res) => {
    try {
        const teamId: string = req.params.teamId;
        const playerId: string = req.params.playerId;
        const result = await suppressPlayerInTeam(teamId, playerId)
        return res.send(result);
    } catch (e){
        return res.status(500).send({error: 'erreur serveur',} + e.message);
    }
});

// Ajouter un player existant d'une Team

app.put('/api/teams/:teamId/players/:playerId', async (req, res) => {
    try {
        const teamId: string = req.params.teamId;
        const playerId: string = req.params.playerId;
        const result = await addExistingPlayerInTeam(teamId, playerId)
        return res.send(result);
    } catch (e){
        return res.status(500).send({error: 'erreur serveur',} + e.message);
    }
});

// Transférer un player existant d'une Team

/*app.put('/api/teams/:teamAUid/:teamBUid/players/:playerToTransferUid', async (req, res) => {
    try {
        const teamAUId: string = req.params.teamAUid;
        const teamBUid: string = req.params.teamBUid;
        const playerToTransferUid: string = req.params.playerToTransferUid;
        const result = await transferPlayerFromAToBTeam(teamAUId, teamBUid, playerToTransferUid)
        return res.send(result);
    } catch (e){
        return res.status(500).send({error: 'erreur serveur',} + e.message);
    }
});*/

app.delete('/api/teams/:teamAUid/players/:playerToTransferUid', async (req, res) => {
    try {
        const teamAUId: string = req.params.teamAUid;
        const playerToTransferUid: string = req.params.playerToTransferUid;
        const result = await transferPlayerFromAToBTeam(teamAUId, teamBUId, playerToTransferUid)
        return res.send(result);
    } catch (e){
        return res.status(500).send({error: 'erreur serveur',} + e.message);
    }
});

app.put('/api/teams/:teamBUid/players/:playerToTransferUid', async (req, res) => {
    try {
        const teamBUId: string = req.params.teamBUId;
        const playerToTransferUid: string = req.params.playerToTransferUid;
        const result = await transferPlayerFromAToBTeam(teamAUId, teamBUId, playerToTransferUid)
        return res.send(result);
    } catch (e){
        return res.status(500).send({error: 'erreur serveur',} + e.message);
    }
});


app.patch('/api/teams/:teamId', async (req, res) => {
    try {
        const teamId: string = req.params.teamId;
        const newTeam: TeamModel = req.body;
        const result = await teamToPatch(teamId, newTeam)
        return res.send(result);
    } catch (e){
        return res.status(500).send({error: 'erreur serveur',} + e.message);
    }
});

app.put('/api/teams/:teamId', async (req, res) => {
    try {
        const teamId: string = req.params.teamId;
        const newTeam: TeamModel = req.body;
        const result = await teamToPut(teamId, newTeam)
        return res.send(result);
    } catch (e){
        return res.status(500).send({error: 'erreur serveur',} + e.message);
    }
});

app.patch('/api/players/:playerId', async (req, res) => {
    try {
        const playerId: string = req.params.playerId;
        const state: PlayerState = req.body.playerState;
        const result = await setPlayerByState(playerId, state)
        return res.send(result);
    } catch (e){
        return res.status(500).send({error: 'erreur serveur',} + e.message);
    }
});

// Récupérer toutes les équipes

app.get('/api/teams', async (req, res)  => {
    try {
        const teams = await getAllTeams();
        return res.send(teams);
    } catch (e){
        return res.status(500).send({error: 'erreur serveur' + e.message});
    }
});


app.get('/api/teams/:id', async (req, res) => {
    try {
        const teamId: string = req.params.id;
        const teamToFind: TeamModel = await getTeamByIdWithAllPlayers(teamId);

        return res.send(teamToFind);
    } catch (e){
        return res.status(500).send({error: 'erreur serveur' + e.message});
    }
});

// Supprimer une Team

app.delete('/api/teams/:teamId', async (req, res) => {
    try {
        const teamId: string = req.params.teamId;
        const result = await deleteTeam(teamId);
        return res.send(result);
    } catch (e){
        return res.status(500).send({error: 'erreur serveur',} + e.message);
    }
});


app.listen(3015, function () {
    console.log('API listening on http://localhost:3015/api/ !');
});

