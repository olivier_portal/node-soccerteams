import admin from "firebase-admin";
import {PlayerModel, PlayerState} from "../model/player.model";
import DocumentReference = FirebaseFirestore.DocumentReference;
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;
import CollectionReference = FirebaseFirestore.CollectionReference;

const db = admin.firestore();
const teamCollection: CollectionReference = db.collection('teams');
const playerCollection: CollectionReference = db.collection('players');

// Récupérer un player par son uid =========================================================

export async function getPayerByUid(playerId: string): Promise<PlayerModel> {
    if (!playerId) {
        throw new Error('playerId does not exist');
    }
    const playerRef: DocumentReference = playerCollection.doc(playerId);
    const playerRefSnap: DocumentSnapshot = await playerRef.get();
    if (!playerRefSnap) {
        throw new Error(`The player with the id ${playerId} does'nt exist`);
    }
    return await playerRefSnap.data() as PlayerModel;
}

// Récupérer des players dans une team par leurs uid=========================================================

export async function getPlayersInTeamByUids(playersUids: string[]) {
    const promisesToResolve: Promise<PlayerModel>[] = playersUids.map((uid) => getPayerByUid(uid));
    return Promise.all(promisesToResolve)
}

// Ajouter un nouveau player dans une team =========================================================

export async function addNewPlayerinTeam(teamId: string, newPlayer: PlayerModel): Promise<string> {
    if (!teamId || !newPlayer) {
        throw new Error('teamId or newPlayer is missing');
    }
    const teamRef: DocumentReference = teamCollection.doc(teamId);
    const teamRefSnap: DocumentSnapshot = await teamRef.get();
    if (!teamRefSnap) {
        throw new Error('team does not exist');
    }
    const createPlayerRef: DocumentReference = await playerCollection.add(newPlayer);
    const createPlayerRefInTeamRef: DocumentReference = teamRef
        .collection('players')
        .doc(createPlayerRef.id);
    await createPlayerRefInTeamRef.set({ref: createPlayerRef, uid: createPlayerRef.id});
    return 'ok';
}

// Supprimer un player d'une team =======================================================

export async function suppressPlayerInTeam(teamId: string, playerId: string): Promise<string> {
    if (!teamId || !playerId) {
        throw new Error('teamId and playerId are required');
    }
    const teamRef: DocumentReference = teamCollection.doc(teamId);
    const teamRefSnap: DocumentSnapshot = await teamRef.get();
    if (!teamRefSnap) {
        throw new Error('team does not exist');
    }
    const playerRefToDelete: DocumentReference = playerCollection.doc(playerId);
    const playerRefToDeleteSnap: DocumentSnapshot = await playerRefToDelete.get();
    const playerToDelete: PlayerModel = playerRefToDeleteSnap.data() as PlayerModel;
    if (!playerToDelete) {
        throw new Error(`The player with the id ${playerId} does'nt exist`);
    }
    const playerRefToDeleteInTeamRef: DocumentReference = teamRef
        .collection('players')
        .doc(playerRefToDelete.id);
    await playerRefToDeleteInTeamRef.delete();
    return 'ok';
}

// Ajouter un player existant dans une team =======================================================

export async function addExistingPlayerInTeam(teamId: string, playerId: string): Promise<string> {
    if (!teamId || !playerId) {
        throw new Error('teamId and playerId are required');
    }
    const teamRef: DocumentReference = teamCollection.doc(teamId);
    const teamRefSnap: DocumentSnapshot = await teamRef.get();
    if (!teamRefSnap) {
        throw new Error('team does not exist');
    }
    const playerRefToAdd: DocumentReference = playerCollection.doc(playerId);
    const playerRefToAddSnap: DocumentSnapshot = await playerRefToAdd.get();
    const playerToAdd: PlayerModel = playerRefToAddSnap.data() as PlayerModel;
    if (!playerToAdd) {
        throw new Error(`The player with the id ${playerId} does'nt exist`);
    }
    const playerRefToAddInTeamRef: DocumentReference = teamRef
        .collection('players')
        .doc(playerRefToAdd.id);
    await playerRefToAddInTeamRef.set({ref: playerRefToAdd, uid: playerRefToAdd.id});
    return 'ok';
}

// Transférer un player d'une team A à B =======================================================

export async function transferPlayerFromAToBTeam(teamAUid: string, teamBUid: string, playerToTransferUid: string): Promise<string> {

    // Vérifier que toutes les données sont renseignées

    if (!teamAUid || !teamBUid || !playerToTransferUid) {
        throw new Error(`teamAUid, teamBUid and playerToTransferUid are all required`)
    }

    // Récupérer l'uid de la team A

    const teamARef: DocumentReference = teamCollection.doc(teamAUid);
    const teamARefSnap: DocumentSnapshot = await teamARef.get();
    if (!teamARefSnap) {
        throw new Error(`teamA doesn't exist`);
    }

    // Récupérer l'uid de la team B

    const teamBRef: DocumentReference = teamCollection.doc(teamAUid);
    const teamBRefSnap: DocumentSnapshot = await teamBRef.get();
    if (!teamBRefSnap) {
        throw new Error(`teamB doesn't exist`);
    }

    // Récupérer l'uid du joueur à transférer

    const playerToTransferRef: DocumentReference = playerCollection.doc(playerToTransferUid);
    const playerToTransferSnap: DocumentSnapshot = await playerToTransferRef.get();
    const playerToTransfer: PlayerModel = playerToTransferSnap.data() as PlayerModel;
    if (!playerToTransfer) {
        throw new Error(`The player with the id ${playerToTransferUid} does'nt exist`);
    }

    // Supprimer le joueur de la team A

    const playerRefToDeleteInTeamARef: DocumentReference = teamARef
        .collection('players')
        .doc(playerToTransferRef.id);
    await playerRefToDeleteInTeamARef.delete();

    // Ajouter le joueur à la team B

    const playerRefToAddInTeamBRef: DocumentReference = teamBRef
        .collection('players')
        .doc(playerToTransferRef.id);
    await playerRefToAddInTeamBRef.set({ref: playerToTransferRef, uid: playerToTransferRef.id});
    return 'ok';
}

// Mettre à jour le statut d'un player =======================================================

// vérifier que le statut existe

export function checkPlayerState(state: any) {
    return Object.keys(PlayerState).some((playerState) => playerState === state);
}

// Faire un patch du state

export async function setPlayerByState(playerId: string, state: PlayerState):Promise<string> {
    if (!playerId) {
        throw new Error(`PlayerId is missing`);
    }
    if (!checkPlayerState(state)) {
        throw new Error('State invalid');
    }
    const playerRef:DocumentReference = playerCollection.doc(playerId);
    const playerRefSnap:DocumentSnapshot = await playerRef.get();
    if (!playerRefSnap) {
        throw new Error(`The player with the id ${playerId} does'nt exist`);
    }
    await playerRef.update({playerState: state});
    return 'ok';
}

export async function getPlayerById(playerId: string) {
    if (!playerId) {
        throw new Error('playerId is missing');
    }
    const playerRef: DocumentReference = playerCollection.doc(playerId);
    const playerSnap:DocumentSnapshot = await playerRef.get();
    if (!playerSnap.exists) {
        throw new Error(`room ${playerId} does not exist`);
    }
    return playerSnap.data() as PlayerModel;
}

export async function getAllPlayersByUids(playersUids: string[]) {
    const promisesToResolve: Promise<PlayerModel>[] = playersUids
        .map((uid) => getPlayerById(uid));
    return await Promise.all(promisesToResolve);
}
