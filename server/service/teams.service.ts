import admin from "firebase-admin";
import {TeamModel} from "../model/team.model";
import DocumentReference = FirebaseFirestore.DocumentReference;
import DocumentSnapshot = FirebaseFirestore.DocumentSnapshot;
import {CollectionReference, QuerySnapshot} from "@google-cloud/firestore";
import {getAllPlayersByUids} from "./players.service";

const db = admin.firestore();
const teamsCollection = db.collection('teams');

export async function findTeamById(teams: TeamModel[], teamId: string): Promise<TeamModel> {
    if (!teamId) {
        throw new Error('teamId does not exist');
    }
    return teams.find((team) => teamId === team.id) as TeamModel;
}

// Get all teams =========================================================

export async function getAllTeams(): Promise<TeamModel[]> {
    const teamToQuerySnap: QuerySnapshot = await teamsCollection.get();
    const hostels: TeamModel[] = [];
    teamToQuerySnap.forEach(teamSnap => hostels.push(teamSnap.data() as TeamModel));
    return hostels;
}


// Post newTeam ================================================================

export async function postNewTeam(newTeam: TeamModel): Promise<TeamModel> {
    if (!newTeam) {
        throw new Error('newTeam must be filled');
    }
    const addResult: DocumentReference = await teamsCollection.add(newTeam);
    return {...newTeam, id: addResult.id};
}


// Patch team =======================================================================

export async function teamToPatch(teamId: string, newTeam: TeamModel): Promise<string> {
    if (!teamId || !newTeam) {
        throw new Error('teamId and newTeam must be filled');
    }
    const teamRef:DocumentReference = teamsCollection.doc(teamId);
    const teamRefSnapToPatch:DocumentSnapshot = await teamRef.get();
    const teamToPatch: TeamModel = teamRefSnapToPatch.data() as TeamModel;
    if (!teamToPatch) {
        throw new Error('This team does not exist');
    }
    await teamRef.update(newTeam);
    return 'ok';
}

// Put team =======================================================================

export async function teamToPut(teamId: string, newTeam: TeamModel): Promise<string> {
    if (!teamId || !newTeam) {
        throw new Error('teamId and newTeam must be filled');
    }
    const teamRef:DocumentReference = teamsCollection.doc(teamId);
    const teamRefSnapToPatch:DocumentSnapshot = await teamRef.get();
    const teamToPut: TeamModel = teamRefSnapToPatch.data() as TeamModel;
    if (!teamToPut) {
        throw new Error('This team does not exist');
    }
    await teamRef.set(newTeam);
    return 'ok';
}

// Get team by Id =======================================================================

export async function getTeamById(teamId: string): Promise<TeamModel> {
    if (!teamId) {
        throw new Error(`teamId required`);
    }

    const teamToFindRef = teamsCollection.doc(teamId);
    const teamToFindSnap: DocumentSnapshot = await teamToFindRef.get();
    if (!teamToFindSnap.exists) {
        throw new Error(`hostel ${teamId} does not exist`);
    } else {
        return teamToFindSnap.data() as TeamModel;
    }
}

// Get team by Id with all players=======================================================================

export async function getTeamByIdWithAllPlayers(teamId: string): Promise<TeamModel> {
    const teamToFind: TeamModel = await getTeamById(teamId);
    const playersCollectionInTeam: CollectionReference = teamsCollection
        .doc(teamId)
        .collection('players');

    const playersCollectionInTeamSnap: QuerySnapshot = await playersCollectionInTeam.get();
    const payersRef: string[] = [];
    playersCollectionInTeamSnap.forEach((playerRef) => payersRef.push(playerRef.data().uid));
    teamToFind.players = await getAllPlayersByUids(payersRef);

    return teamToFind;
}

// Delete team by Id=======================================================================

export async function deleteTeam(teamId: string): Promise<any> {
    if (!teamId) {
        throw new Error(`This team doesn't exist`);
    }

    const teamRef: DocumentReference = teamsCollection.doc(teamId);
    const snapTeamToDelete: DocumentSnapshot = await teamRef.get();
    const teamToDelete: TeamModel | undefined = snapTeamToDelete.data() as TeamModel | undefined;
    if (!teamToDelete) {
        return `The hostel with the id ${teamId} doesn't exist`;
    }
    await teamRef.delete();
    return {response: `The hostel with the id ${teamId} has been deleted successfully`};
}
